package Chat_Tests;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;



public class Skype_Chat_SendImage_Test {

    private static AndroidDriver driver;
    public static Object WebDriverWait;

    public static void main(String args[]) throws IOException, InterruptedException {

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        dc.setCapability("platformName", "android");
        dc.setCapability("appPackage", "com.skype.raider");
        dc.setCapability("appActivity", ".Main");
        dc.setCapability("noReset", true);

        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);

        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);

        MobileElement el1 = (MobileElement) driver.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Contacts, Tab 3 of 3\"]/android.view.ViewGroup");
        el1.click();
        MobileElement el2 = (MobileElement) driver.findElementByXPath("//android.widget.Button[@content-desc=\"Miks Dambrovskis, Away, , Double tap and hold to bring up more options\"]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView");
        el2.click();
        MobileElement el3 = (MobileElement) driver.findElementByXPath("//android.widget.Button[@content-desc=\"Open camera and media file picker\"]/android.view.ViewGroup");
        el3.click();
        MobileElement el5 = (MobileElement) driver.findElementByXPath("//android.widget.Button[@content-desc=\"Open gallery\"]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.ImageView");
        el5.click();
        MobileElement el6 = (MobileElement) driver.findElementByAccessibilityId("Gallery item 1 of 1, photo");
        el6.click();
        MobileElement el7 = (MobileElement) driver.findElementByXPath("//android.widget.Button[@content-desc=\"Next\"]/android.widget.TextView");
        el7.click();
        MobileElement el8 = (MobileElement) driver.findElementByXPath("//android.widget.Button[@content-desc=\"Send to Miks Dambrovskis.\"]/android.view.ViewGroup[2]/android.widget.TextView");
        el8.click();

        Thread.sleep(5000);



    }

}




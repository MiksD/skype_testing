package Call_Tests;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Skype_Call_Test {

    private static AndroidDriver driver;
    public static Object WebDriverWait;

    public static void main(String args[]) throws MalformedURLException, InterruptedException {

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        dc.setCapability("platformName", "android");
        dc.setCapability("appPackage", "com.skype.raider");
        dc.setCapability("appActivity", ".Main");
        dc.setCapability("noReset", true);

        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);

        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);

        MobileElement el1 = (MobileElement) driver.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Contacts, Tab 3 of 3\"]/android.view.ViewGroup");
        el1.click();
        MobileElement el2 = (MobileElement) driver.findElementByXPath("//android.widget.Button[@content-desc=\"Echo / Sound Test Service ., Active now, , Double tap and hold to bring up more options\"]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView");
        el2.click();
        MobileElement el3 = (MobileElement) driver.findElementByXPath("//android.widget.Button[@content-desc=\"Audio Call\"]/android.widget.TextView");
        el3.click();

        Thread.sleep(15000);

        Assert.assertEquals(driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.View").isDisplayed(), true);

        Thread.sleep(5000);



    }

}


